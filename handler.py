import argparse
import os
from collections import defaultdict, OrderedDict
from hashlib import md5

parser = argparse.ArgumentParser()
parser.add_argument("dir_input", nargs='?')
args = parser.parse_args()

if args.dir_input:
    files_storage = defaultdict(list)
    print("Enter file format:")
    file_format = input()

    print("""
Size sorting options:
1. Descending
2. Ascending
    """)

    while True:
        print("Enter a sorting option:")
        sorting_option = int(input())
        if sorting_option in (1, 2):
            break
        else:
            print("Wrong option")

    for root, dirs, files in os.walk(args.dir_input):
        for name in files:
            file_name = os.path.join(root, name)
            file_size = os.path.getsize(file_name)
            with open(file_name, "rb") as file_input:
                file_hash = md5(file_input.read()).hexdigest()
            files_storage[file_size].append((file_name, file_hash))
            # print(file_name)

    size_order = sorted(files_storage, reverse=(sorting_option == 1))
    same_hash = {}
    for size in size_order:
        files = files_storage[size]
        print(size, "bytes", sep=" ")
        same_hash[size] = defaultdict(list)
        for file, hash_file in files:
            if file_format:
                if file.endswith(file_format):
                    print(file)
                    same_hash[size][hash_file].append(file)
            else:
                print(file)
                same_hash[size][hash_file].append(file)
        print()

    while True:
        print("Check for duplicates?")
        duplicate_check = input()
        if duplicate_check in ("yes", "no"):
            break
        else:
            print("Wrong option")

    if duplicate_check == "yes":
        duplicates_files = OrderedDict()
        counter = 1
        for size, hash_dict in same_hash.items():
            duplicates_files[size] = OrderedDict()
            for hash_element, files in hash_dict.items():
                if len(files) > 1:
                    duplicates_files[size][hash_element] = []
                    for file_x in files:
                        duplicates_files[size][hash_element].append((counter, file_x))
                        counter += 1
            if not duplicates_files[size]:
                del duplicates_files[size]
        for size, hash_dict in duplicates_files.items():
            print(f"{size} bytes")
            for hash_element, files in hash_dict.items():
                print(f"Hash: {hash_element}")
                for counter, file_element in files:
                    print(f"{counter}. {file_element}")

    while True:
        print("Delete files?")
        delete_files = input()
        if duplicate_check in ("yes", "no"):
            break
        else:
            print("Wrong option")
    if delete_files == "yes":
        while True:
            print("Enter file numbers to delete:")
            try:
                index_delete = [int(x) for x in input().split()]
                if not index_delete:
                    raise ValueError
                for x in index_delete:
                    if x not in list(range(1, counter+1)):
                        raise ValueError
                break
            except ValueError:
                print("Wrong option")

        total_size = 0
        for size, hash_dict in duplicates_files.items():
            for hash_element, files in hash_dict.items():
                for counter, file_element in files:
                    if counter in index_delete:
                        os.unlink(file_element)
                        total_size += size
        print(f"Total freed up space: {total_size} bytes")
else:
    print("Directory is not specified")
